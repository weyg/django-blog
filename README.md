# Django-blog

For production (Heroku) you need to set:
* `SECRET_KEY`, 
* `DJANGO_APP_ENV=production`

Heroku will also provide `DATABASE_URL`, which you need to provide yourself for your Postgres database if you are in some other environment.
