from os import environ

from .common import *

if environ.get('DJANGO_APP_ENV') == 'production':
    from .production import *
else:
    from .development import *
