# Generated by Django 4.0.2 on 2022-02-17 18:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_user_can_comment_user_can_post'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='mod_date',
            field=models.DateTimeField(null=True, verbose_name='date modified'),
        ),
        migrations.AddField(
            model_name='blog',
            name='private',
            field=models.BooleanField(default=False),
        ),
    ]
