from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.views import generic
from blog.models import Blog, User
from django.utils import timezone
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib import messages
from django.urls import reverse
from blog.forms import BlogForm

def index(request):
    curr_blog = Blog.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')[:1]
    previous = None
    next = None
    if curr_blog:
        previous = Blog.objects.filter(pub_date__lt=curr_blog[0].pub_date).order_by('-pub_date')[:1]
        next = Blog.objects.filter(pub_date__lte=timezone.now()).filter(pub_date__gt=curr_blog[0].pub_date).order_by('pub_date')[:1]

    context = {
        'curr_blog': curr_blog, 
        'previous': previous,
        'next': next,
        }

    return render(request, 'index.html', context)

def view_post(request, blog_id):
    curr_blog = Blog.objects.filter(id=blog_id).filter(pub_date__lte=timezone.now()).order_by('-pub_date')[:1]
    previous = None
    next = None
    if curr_blog:
        previous = Blog.objects.filter(pub_date__lt=curr_blog[0].pub_date).order_by('-pub_date')[:1]
        next = Blog.objects.filter(pub_date__lte=timezone.now()).filter(pub_date__gt=curr_blog[0].pub_date).order_by('pub_date')[:1]

    context = {
        'curr_blog': curr_blog, 
        'previous': previous,
        'next': next,
        }

    return render(request, 'index.html', context)

def create_blog(request):
    if request.method == 'POST':
        form = BlogForm(request.POST)
    else:
        form = BlogForm()
    return render(request, 'blog/create.html', {'form': form})

def register_user(request):

    class CustomUserCreationForm(UserCreationForm):

        class Meta(UserCreationForm.Meta):
            model = User
            fields = UserCreationForm.Meta.fields
            help_texts = {
                'username': None,
            }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['password1'].help_text = None

    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            messages.success(request, 'Registration successful!')
            login(request, user)
            messages.info(request, 'Login successful, welcome {}!'.format(user.username))
            return redirect('blog:index')
    else:
        form = CustomUserCreationForm()
    return render(request, 'registration/register.html', {'form': form})
