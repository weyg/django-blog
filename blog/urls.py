from django.urls import path, include

from . import views

app_name = 'blog'
urlpatterns = [
    path('', views.index, name='index'),
    path('blog/<int:blog_id>', views.view_post, name='blog'),
    path('blog/create', views.create_blog, name='create_blog'),
    path('register', views.register_user, name='register_user'),
]