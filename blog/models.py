from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone

class User(AbstractUser):
    pass

class Blog(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    blog_title = models.CharField(max_length=200)
    blog_text = models.TextField()
    pub_date = models.DateTimeField('date published')
    mod_date = models.DateTimeField('date modified', null=True, blank=True)
    private = models.BooleanField(default=False)

    def __str__(self):
        return self.blog_title
