from django import forms
from datetime import date

class BlogForm(forms.Form):
    title = forms.CharField(label='Blog title', max_length=200, min_length=3, strip=True)
    content = forms.CharField(label='Content', widget=forms.Textarea, strip=True, required=False)
    pub_date = forms.DateField(label='Publishing date', widget=forms.SelectDateWidget, required=False, initial=date.today)
