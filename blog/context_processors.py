
def navigation_context_processor(request):
    # name, method, always, user is auth, user is contrib
    context = {
        'navigation': [('Home', 'blog:index', True, False, False),
        ('Archive', 'blog:index', True, False, False),
        ('Manage', 'blog:index', False, True, True),
        ('Login', 'login', False, False, False),
        ('Logout', 'logout', False, True, False)]
    }
    return context